terraform {
  required_version = ">= 1.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.35"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.25"
    }
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.12"
    }
    kubectl = {
      source  = "alekc/kubectl"
      version = ">= 2.0.4"
    }
    null = {
      source  = "hashicorp/null"
      version = ">= 3.0"
    }
    flux = {
      source  = "fluxcd/flux"
      version = "~> 1.2.3"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = ">=16.8.0"
    }
  }
}
