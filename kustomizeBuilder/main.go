package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"sync"
)

type Config struct {
	Regions      []string `json:"regions"`
	Environments []string `json:"environments"`
	Services     []string `json:"services"`
}

func main() {
	// Read the JSON file
	data, err := os.ReadFile("config.json")
	if err != nil {
		log.Fatal(err)
	}

	// Unmarshal the JSON data into a Config struct
	var config Config
	err = json.Unmarshal(data, &config)
	if err != nil {
		log.Fatal(err)
	}

	// Create each directory
	var wg sync.WaitGroup
	for _, region := range config.Regions {
		for _, environment := range config.Environments {
			for _, service := range config.Services {
				dir := fmt.Sprintf("config/%s/%s/%s", region, environment, service)
				done := make(chan bool)
				go createDir(dir, done)
				wg.Add(1)
				go copyFiles([]string{"kustomization.yaml", "values.yaml"}, dir, &wg, done)
			}
		}
	}
	wg.Wait()
}

func copyFiles(srcPaths []string, destDir string, wg *sync.WaitGroup, done <-chan bool) error {
	defer wg.Done()

	// Wait for the directory to be created
	<-done

	for _, srcPath := range srcPaths {
		// Check if the source file exists
		if _, err := os.Stat(destDir); os.IsNotExist(err) {
			log.Printf("Source file %s does exist, skipping\n", srcPath)
			continue
		}

		// Open the source file
		srcFile, err := os.Open(srcPath)
		if err != nil {
			return err
		}
		defer srcFile.Close()

		// Create the destination file
		destPath := fmt.Sprintf("%s/%s", destDir, filepath.Base(srcPath))
		destFile, err := os.Create(destPath)
		if err != nil {
			return err
		}
		defer destFile.Close()

		// Copy the file
		_, err = io.Copy(destFile, srcFile)
		if err != nil {
			return err
		}
	}
	return nil
}

func createDir(dir string, done chan<- bool) {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err = os.MkdirAll(dir, 0755)
		if err != nil {
			log.Printf("Error creating directory %s: %v\n", dir, err)
			return
		}
	} else {
		log.Printf("Directory %s already exists\n", dir)
	}
	done <- true
}
