# domain-infra



## Purpose
This should serve for any cluster related infra (think core services that will be deployed and used by all apps) and any supporting infra.


Initially I tested using pulumi, Although I had high hope, it is proving to be troublesome vs the out of the box working eks cluster I got with EKS. I would like to come back to pulumi go but I have burned too many cycles to get the benefits