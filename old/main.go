package main

import (
	"fmt"

	"github.com/pulumi/pulumi-aws/sdk/v4/go/aws"
	"github.com/pulumi/pulumi-aws/sdk/v4/go/aws/ec2"

	"github.com/pulumi/pulumi-eks/sdk/go/eks"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
)

func main() {
	pulumi.Run(func(ctx *pulumi.Context) error {

		// TODO - move to network package

		//Create a vpc for the cluster
		vpc, err := ec2.NewVpc(ctx, "my-vpc", &ec2.VpcArgs{
			CidrBlock: pulumi.String("10.0.0.0/16"),
		})
		if err != nil {
			return err
		}

		// Get the list of availability zones
		azs, err := aws.GetAvailabilityZones(ctx, nil, nil)
		if err != nil {
			return err
		}

		// Create a subnet in each availability zone
		subnetIds := make([]pulumi.StringInput, len(azs.Names))
		for i, az := range azs.Names {
			subnet, err := ec2.NewSubnet(ctx, fmt.Sprintf("my-subnet-%d", i), &ec2.SubnetArgs{
				CidrBlock:        pulumi.String(fmt.Sprintf("10.0.%d.0/24", i)),
				VpcId:            vpc.ID(),
				AvailabilityZone: pulumi.String(az),
			})
			if err != nil {
				return err
			}
			subnetIds[i] = subnet.ID()
		}

		// TODO - extract all vars to config
		SkipDefaultNodeGroup := true

		// Create an EKS cluster with the default settings.
		cluster, err := eks.NewCluster(ctx, "my-cluster", &eks.ClusterArgs{
			SkipDefaultNodeGroup: &SkipDefaultNodeGroup,
			VpcId:                vpc.ID(),
			SubnetIds:            pulumi.StringArray(subnetIds),
			Version:              pulumi.String("1.29"),
			// Fargate:              pulumi.Bool(true),
		},
		)
		if err != nil {
			return err
		}

		// 	// Create the IAM role for Karpenter
		// 	role, err := iam.NewRole(ctx, "karpenterRole", &iam.RoleArgs{
		// 		AssumeRolePolicy: pulumi.String(`{
		//     "Version": "2012-10-17",
		//     "Statement": [
		//         {
		//             "Action": "sts:AssumeRole",
		//             "Principal": {
		//                 "Service": "ec2.amazonaws.com"
		//             },
		//             "Effect": "Allow",
		//             "Sid": ""
		//         }
		//     ]
		// }`),
		// 	})
		// 	if err != nil {
		// 		return err
		// 	}

		// 	// Attach the necessary IAM policies to the role
		// 	_, err = iam.NewRolePolicyAttachment(ctx, "karpenterRolePolicyAttachment", &iam.RolePolicyAttachmentArgs{
		// 		Role:      role.Name,
		// 		PolicyArn: pulumi.String("arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"),
		// 	})
		// 	if err != nil {
		// 		return err
		// 	}

		// Pass the kubeconfig to the Helm provider.
		// kubeconfig := cluster.Kubeconfig.ApplyT(func(kc interface{}) (string, error) {
		// 	kubeconfigMap := kc.(map[string]interface{})
		// 	data, err := json.Marshal(kubeconfigMap)
		// 	if err != nil {
		// 		return "", err
		// 	}
		// 	str := string(data)
		// 	return str, nil
		// }).(pulumi.StringOutput)

		// provider, err := kubernetes.NewProvider(ctx, "k8s-provider", &kubernetes.ProviderArgs{
		// 	Kubeconfig: kubeconfig,
		// })
		// if err != nil {
		// 	return err
		// }
		// _, err = helm.NewChart(ctx, "nginx-ingress", helm.ChartArgs{
		// 	Chart:     pulumi.String("nginx-ingress"),
		// 	Version:   pulumi.String("1.41.3"),
		// 	Namespace: pulumi.String("kube-system"),
		// 	Values: pulumi.Map{
		// 		"controller": pulumi.Map{
		// 			"publishService": pulumi.Map{
		// 				"enabled": pulumi.Bool(true),
		// 			},
		// 		},
		// 	},
		// 	FetchArgs: helm.FetchArgs{
		// 		Repo: pulumi.String("https://charts.helm.sh/stable"),
		// 	},
		// }, pulumi.Provider(provider))
		// if err != nil {
		// 	return err
		// }

		ctx.Export("kubeconfig", cluster.Kubeconfig)

		return nil
	},
	)
}
