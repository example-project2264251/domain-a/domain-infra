import * as pulumi from "@pulumi/pulumi";
import {cluster} from "../src/cluster.js";
import { expect } from "chai";
import "mocha";

pulumi.runtime.setMocks(
  {
    newResource: function (
      args: pulumi.runtime.MockResourceArgs
    ): pulumi.runtime.MockResourceResult {
      // Mock implementation of newResource
      console.log(`Creating resource ${args.name} of type ${args.type}`);
      return {
        id: args.inputs.name + "_id",
        state: args.inputs,
      };
    },
    call: function (args: pulumi.runtime.MockCallArgs): any {
      // Mock implementation of call
      console.log(`Calling function ${args.token}`);
      return args;
    },
  },
  "project",
  "stack",
  true
);

describe("Infrastructure", function () {
  let infra: typeof import("../src/cluster.js");

  before(async function () {
    // It's important to import the program _after_ the mocks are defined.
    infra = await import ("../src/cluster.js");
  });

  describe("Cluster",async function () {
    const clusterName = "my-cluster"; // replace with your actual cluster name
    const vpcId = pulumi.output("vpc-123"); // replace with your actual VPC ID
    const subnetIds = [
      pulumi.output("subnet-123"),
      pulumi.output("subnet-456"),
    ]; // replace with your actual subnet IDs
    const instanceType = "t2.micro"; // replace with your actual instance type
    const desiredCapacity = 2; // replace with your actual desired capacity
    const minSize = 1; // replace with your actual min size
    const maxSize = 3; // replace with your actual max size
    const deployDashboard = true; // replace with your actual deploy dashboard value
    const createOidcProvider = true; // replace with your actual create OIDC provider value
    const eksTags = { Name: "my-cluster" }; // replace with your actual EKS tags

    it("should have correct cluster name", function () {
      pulumi.all([cluster.eksCluster.name]).apply(([clusterName]) => {
        expect(cluster.eksCluster.name.get).to.equal(clusterName);
      });
    });

    it("should have correct vpc id", function () {
      pulumi
        .all([infra.cluster.eksCluster.vpcConfig.vpcId])
        .apply(([vpcId]) => {
          expect(infra.cluster.eksCluster.vpcConfig.vpcId).to.equal(vpcId);
        });
    });

    it("should have correct subnet ids", function () {
      pulumi
        .all([cluster.eksCluster.vpcConfig.subnetIds])
        .apply(([subnetIds]) => {
          expect(infra.cluster.eksCluster.vpcConfig.subnetIds).to.equal(
            subnetIds
          );
        });
    });

    it("should have correct arn", function () {
      pulumi.all([infra.cluster.eksCluster.arn]).apply(([clusterArn]) => {
        expect(infra.cluster.eksCluster.arn).to.equal("hello");
      });
    });
  });
});
// describe('Cluster Tests', () => {
//     let cluster;
//     before(async () => {
//         cluster = await createCluster("clusterName", pulumi.output("vpc-123"), [pulumi.output("subnet-123"), pulumi.output("subnet-456")], "instanceType", 2, 1, 3, true, true, {"tagKey": "tagValue"});
//     });

//     it('creates a cluster', () => {
//         // Your test assertions here
//     });
// });

// describe("EKS Cluster", function () {

//     before(async function () {
//         pulumi.runtime.setMocks({
//             newResource: function(args: pulumi.runtime.MockResourceArgs): pulumi.runtime.MockResourceResult {
//                 return {
//                     id: args.inputs.name + "_id",
//                     state: args.inputs,
//                 };
//             },
//             call: function(args: pulumi.runtime.MockCallArgs): any {
//                 return args.inputs;
//             },
//         });
//     });

//     it("should have correct cluster name", async function () {
//         const clusterName = "my-cluster"; // replace with your actual cluster name
//         const vpcId = pulumi.output("vpc-123"); // replace with your actual VPC ID
//         const subnetIds = [pulumi.output("subnet-123"), pulumi.output("subnet-456")]; // replace with your actual subnet IDs
//         const instanceType = "t2.micro"; // replace with your actual instance type
//         const desiredCapacity = 2; // replace with your actual desired capacity
//         const minSize = 1; // replace with your actual min size
//         const maxSize = 3; // replace with your actual max size
//         const deployDashboard = true; // replace with your actual deploy dashboard value
//         const createOidcProvider = true; // replace with your actual create OIDC provider value
//         const eksTags = { "Name": "my-cluster" }; // replace with your actual EKS tags

//         try {
//             const cluster = createCluster(clusterName, vpcId, subnetIds, instanceType, desiredCapacity, minSize, maxSize, deployDashboard, createOidcProvider, eksTags);
//             expect(cluster).to.equal(clusterName);
//         } catch (error) {
//             // If there was an error creating the cluster, it will be logged here
//             console.error(error);
//         }
//     });
// });
