import * as pulumi from "@pulumi/pulumi";
import { vpcId, subnetIds } from "./network.js";
import * as eks from "@pulumi/eks";

// Create an EKS cluster with the default configuration.
const cluster = new eks.Cluster("cluster", {
    vpcId: vpcId,
    subnetIds: subnetIds,
});

// Export the cluster's kubeconfig.
export const kubeconfig = cluster.kubeconfig;