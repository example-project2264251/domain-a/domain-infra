import * as fs from 'fs';
import * as pulumi from '@pulumi/pulumi';
import * as yaml from 'js-yaml';

// Define the Namespace using a JavaScript object
const namespace = {
  apiVersion: 'v1',
  kind: 'Namespace',
  metadata: {
    name: 'test-namespace',
  },
};

// Convert the JavaScript object to a YAML string using js-yaml
const namespaceYaml = pulumi.output(namespace).apply(n => yaml.dump(n));

// Write the generated YAML to a file
namespaceYaml.apply((content: string | NodeJS.ArrayBufferView) => {
  fs.writeFileSync('namespace.yaml', content);
});

// Export the content of the YAML file as a stack output (optional)
export const namespaceYamlContent = namespaceYaml;