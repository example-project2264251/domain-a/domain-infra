// using network.ts create a eks cluster

import { Cluster } from "@pulumi/eks";
import { vpcId, subnetIds } from "./network.js";
// import "./yaml.js"
import { Output } from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import * as kx from "@pulumi/kubernetesx";
import * as random from "@pulumi/random";

const clusterName = "eks-cluster";
const instanceType = "t2.medium";
const desiredCapacity = 2;
const minSize = 1;
const maxSize = 3;
const deployDashboard = false;
const createOidcProvider = false;

const eksTags = {
    Name: "pulumi-eks-cluster",
};

// Create an EKS cluster with the default node group in the custom VPC and Subnets.
// export const cluster: Cluster = await createCluster(clusterName, vpcId, subnetIds, instanceType, desiredCapacity, minSize, maxSize, deployDashboard, createOidcProvider, eksTags) as Cluster;
export const cluster: Cluster = new Cluster(clusterName, {
    vpcId: vpcId,
    subnetIds: subnetIds,
    // instanceType: instanceType,
    // desiredCapacity: desiredCapacity,
    // minSize: minSize,
    // maxSize: maxSize,
    // deployDashboard: deployDashboard,
    // createOidcProvider: createOidcProvider,
    // tags: eksTags,
    version: "1.29",
    
});

// export function createCluster(clusterName: string, vpcId: Output<string>, subnetIds: Output<string>[], instanceType: string, desiredCapacity: number, minSize: number, maxSize: number, deployDashboard: boolean, createOidcProvider: boolean, tags: { [key: string]: string } = {}) {

//     return new Promise((resolve, reject) => {

//         resolve(new Cluster(clusterName, {
//             vpcId: vpcId,
//             subnetIds: subnetIds,
//             instanceType: instanceType,
//             desiredCapacity: desiredCapacity,
//             minSize: minSize,
//             maxSize: maxSize,
//             deployDashboard: deployDashboard,
//             createOidcProvider: createOidcProvider,
//             tags: eksTags,
//         }));
//         reject(new Error("Error creating cluster"));
//     });
// }
// Export the cluster's kubeconfig.
export const kubeconfig = cluster.kubeconfig;

// export cluster for testing
export const clusterTest = cluster.eksCluster;


// Instantiate a Kubernetes Provider and specify the render directory.
const provider = new k8s.Provider("render-yaml", {
    renderYamlToDirectory: "rendered",
        
});

// Create a Kubernetes PersistentVolumeClaim.
const pvc = new kx.PersistentVolumeClaim("data", {
    spec: {
        accessModes: [ "ReadWriteOnce" ],
        resources: { requests: { storage: "1Gi" } },
    }
}, { provider });

// Create a Kubernetes ConfigMap.
const cm = new kx.ConfigMap("cm", {
    data: { "config": "very important data" },
}, { provider });

// Create a Kubernetes Secret.
const secret = new kx.Secret("secret", {
    stringData: {
        "password": new random.RandomPassword("pw", {
            length: 12}).result,
    }
}, { provider });

// Define a Pod.
const pb = new kx.PodBuilder({
    containers: [{
        env: {
            CONFIG: cm.asEnvValue("config"),
            PASSWORD: secret.asEnvValue("password"),
        },
        image: "nginx",
        ports: {http: 8080},
        volumeMounts: [ pvc.mount("/data") ],
    }]
});

// Create a Kubernetes Deployment.
const deployment = new kx.Deployment("nginx", {
    spec: pb.asDeploymentSpec( { replicas: 3 } ),
}, { provider });

// Create a Kubernetes Service.
const service = deployment.createService({
    type: kx.types.ServiceType.LoadBalancer,
});

// const node = new k8s.helm.v2.Chart(
//     "node",
//     {
//         repo: "bitnami",
//         chart: "node",
//         version: "4.0.1",
//         values: {
//             serviceType: "LoadBalancer",
//             mongodb: { install: false },
//         }
//     },
//     { provider }
// );

// const wordpress = new k8s.helm.v3.Chart(
//     "wpdev", 
//     {
//     version: "15.0.5",
//     chart: "wordpress",
//     fetchOpts: {
//         repo: "https://charts.bitnami.com/bitnami",
//     },
// }, { provider });
