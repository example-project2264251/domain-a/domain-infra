import {ec2} from "@pulumi/aws";

// Create a VPC for our cluster.
const vpc = new ec2.Vpc("vpc", {
    cidrBlock: "10.100.0.0/16",
    enableDnsHostnames: true,
    enableDnsSupport: true,
    tags: {
        Name: "pulumi-eks-vpc",
    },
});

const subnets = ["eu-west-1a", "eu-west-1b", "eu-west-1c"].map((zone, index) => {
    return new ec2.Subnet(`subnet-${index + 1}`, {
      vpcId: vpc.id,
      cidrBlock: `10.100.${index + 1}.0/24`,
      availabilityZone: zone,
      tags: {
        Name: `pulumi-vpc-subnet-${index + 1}`,
      },
    });
  });

export const vpcId = vpc.id;
export const subnetIds = subnets.map(subnet => subnet.id);
